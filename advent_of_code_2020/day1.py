# Find the two entries that sum to 2020;
# what do you get if you multiply them together?
# Hint: use day1_input file

# if you subtract the first value from 2020, is the result in the list?

# does the first value and second value = 2020
# if not, first value and third value = 2020
# etc.
# how to keep first value and iterate thru list?




test_expenses = [979, 1721, 366, 299]
target_sum = 2020
expenses_input = open('../general_repo/advent_of_code_2020/day1_input').read().splitlines()

while len(expenses_input) > 0:

    check_value = int(expenses_input.pop(0))
    # print(check_value)
    search_value = target_sum-check_value
    if str(search_value) in expenses_input:
        print(search_value)

    # ====================== either option
    for item in expenses_input:
        if int(item) == search_value:
            print(item, check_value)
            print(search_value*check_value)
            break

