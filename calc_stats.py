# Calc Stats:
# Your task is to process a sequence of integer numbers to determine the following statistics:

 

#     o) minimum value
#     o) maximum value
#     o) number of elements in the sequence
#     o) average value

 

# For example: [6, 9, 15, -2, 92, 11]

 

#     o) minimum value = -2
#     o) maximum value = 92
#     o) number of elements in the sequence = 6
#     o) average value = 21.833333
 

# Output the min, max, number and average to the screen.


# Source:  https://cyber-dojo.org/

import math
from statistics import mean

integer_list = [3, 10, -15, 83, -3, 37, 5]

minimum_value = min(integer_list)
maximum_value = max(integer_list)
element_count = len(integer_list)
average_value = mean(integer_list)

print("Minimum Value is: ", minimum_value)
print("Maximum Value is: ", maximum_value)
print("There are", element_count, "", "elements in the list" )
print("The Mean or Average Value, rounded to the tenths is: ", round(average_value, 1))


# Bonus: Create a function that will return a single result when you pass the sequence of numbers and the value you want (minimum, maximum, number of elements, average)
# def calc_stat():
#     integer_list = [3, 10, -15, 83, -3, 37, 5]

#     minimum_value = min(integer_list)
#     maximum_value = max(integer_list)
#     element_count = len(integer_list)
#     average_value = mean(integer_list)

#     print("Minimum Value is: ", minimum_value)
#     print("Maximum Value is: ", maximum_value)
#     print("There are", element_count, "", "elements in the list" )
#     print("The Mean or Average Value, rounded to the tenths is: ", round(average_value, 1))

# calc_stat()


# Bonus: Write tests for this program using pytest or similar.

# Bonus: Write in a language other than Python